#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cstdlib>
#include <mpi.h>

using namespace std;

#define CUDA_CHECK(__err) \
do { \
    if (__err != cudaSuccess) { \
        fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
        fprintf(stderr, "*** FAILED - ABORTING\n"); \
        exit(EXIT_FAILURE); \
    } \
} while (0)

#define CUBLASS_CHECK(__err) \
if (__err != CUBLAS_STATUS_SUCCESS) {\
        cublas_check(__err); \
        fprintf(stderr, "Fatal error at %s:%d\n", __FILE__, __LINE__); \
        fprintf(stderr, "*** FAILED - ABORTING\n"); \
        exit(EXIT_FAILURE); \
}

void cublas_check(cublasStatus_t error){
    switch (error)
    {
        case CUBLAS_STATUS_SUCCESS:
            break;

        case CUBLAS_STATUS_NOT_INITIALIZED:
            printf("CUBLAS_STATUS_NOT_INITIALIZED\n");
            break;

        case CUBLAS_STATUS_ALLOC_FAILED:
            printf("CUBLAS_STATUS_ALLOC_FAILED\n");
            break;

        case CUBLAS_STATUS_INVALID_VALUE:
            printf("CUBLAS_STATUS_INVALID_VALUE\n");
            break;

        case CUBLAS_STATUS_ARCH_MISMATCH:
            printf("CUBLAS_STATUS_ARCH_MISMATCH\n");
            break;

        case CUBLAS_STATUS_MAPPING_ERROR:
            printf("CUBLAS_STATUS_MAPPING_ERROR\n");
            break;

        case CUBLAS_STATUS_EXECUTION_FAILED:
            printf("CUBLAS_STATUS_EXECUTION_FAILED\n");
            break;

        case CUBLAS_STATUS_INTERNAL_ERROR:
            printf("CUBLAS_STATUS_INTERNAL_ERROR\n");
            break;
    }
}

template <typename T>
T function(T x){
  return x*x/2.0;
}

template <typename T, int n>
void laplacjan_matrix(T *A, int ip, int np){
  int m = n/np;
  size_t size = sizeof(double)*n *m;;
  //memset(A, 0, size);
  for(int i = ip*m; i < (ip+1)*m; i++){
    for(int j = 0 ; j < n; j++){
      int it = i-ip*m + j*n;
      if(i == j) A[it] = -2;
      else if(abs(i-j) == 1) A[it] = 1;
      else A[it] = 0;
    }
  }
}

int main(int argc, char *argv[]){
  double t1, t2;
  int ip, np, m;
  int lrank = 0;
  int gpu_count = 0;
  MPI_Init( &argc , &argv );
  MPI_Comm_size( MPI_COMM_WORLD , &np );
  MPI_Comm_rank( MPI_COMM_WORLD , &ip );
  CUDA_CHECK(cudaGetDeviceCount(&gpu_count));
  lrank = ip % gpu_count;
  CUDA_CHECK(cudaSetDevice(lrank));
  MPI_Barrier(MPI_COMM_WORLD);

  cublasHandle_t handle;
  CUBLASS_CHECK(cublasCreate(&handle));

  m = N/np;
  size_t size = sizeof(double)*N *m;;
  double *A, *B, *X;
  A = (double *) malloc(size);
  B = (double *) malloc(sizeof(double)*N);
  X = (double *) malloc(sizeof(double)*m);
  for(int i = 0; i < N; i++){
    B[i] = function<double>((double)(i-N/2));
  }
  laplacjan_matrix<double,N>(A, ip, np);
  MPI_Barrier(MPI_COMM_WORLD);
  t1 = MPI_Wtime();
  double *d_a = 0;
  double *d_b = 0;
  double *d_x = 0;
  for(int i = 0; i < m; i++){
    X[i] = 0;
  }
  CUDA_CHECK(cudaMalloc((void **)&d_b, N*sizeof(double)));
  CUDA_CHECK(cudaMalloc((void **)&d_x, m*sizeof(double)));
  CUDA_CHECK(cudaMalloc((void **)&d_a, m*N*sizeof(double)));
  CUDA_CHECK(cudaMemcpy(d_b, B, N*sizeof(double), cudaMemcpyHostToDevice));
  CUDA_CHECK(cudaMemcpy(d_x, X, m*sizeof(double), cudaMemcpyHostToDevice));
  CUDA_CHECK(cudaMemcpy(d_a, A, m*N*sizeof(double), cudaMemcpyHostToDevice));

  float alpha = 0.1f;
  float beta = 0.1f;

  CUBLASS_CHECK(cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_HOST));

  CUBLASS_CHECK(cublasSgemv(handle,
      CUBLAS_OP_N,
      m,
      N,
      &alpha,
      d_a,
      m,
      d_b,
      1,
      &beta,
      d_x,
      1
  ));

  CUDA_CHECK(cudaMemcpy(X+ip*m, d_x, m*sizeof(double), cudaMemcpyDeviceToHost));
  t2 = MPI_Wtime() - t1;
  MPI_Barrier(amgx_mpi_comm);
  MPI_Gather(X+ip*m, m, MPI_DOUBLE, X, m, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  t3 = MPI_Wtime() - t1 - t2;

  MPI_Offset offset; MPI_File fh ; MPI_Status status ;
  offset = N *m;
  MPI_File_open (MPI_COMM_WORLD, "data4.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  MPI_Offset displace = offset * ip *sizeof(double);
  MPI_File_set_view (fh , displace , MPI_DOUBLE, MPI_DOUBLE, "native" ,MPI_INFO_NULL);
// note that etype and filetype are the same

  MPI_File_write(fh, A, offset, MPI_DOUBLE, &status);

  MPI_File_close(&fh);

  //cout<<"DDDDD"<<endl;

  if (ip == 0){
    cout<<"Czas II: "<<t2<<" , czas III: "<<t3<<endl;
    FILE *fp = fopen("result.dat", "w");
    fwrite(X, sizeof(double), N, fp);
    fclose(fp);
  }

  //cout<<"EEEEE"<<endl;
  MPI_Barrier(MPI_COMM_WORLD);

  CUDA_CHECK(cudaFree(d_x));
  CUDA_CHECK(cudaFree(d_a));
  CUDA_CHECK(cudaFree(d_b));
  MPI_Finalize();
  return 0;
}
