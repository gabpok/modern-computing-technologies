#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>

/*
mpic++ go_array_mpi.c -O3 -o go_array_mpi -lm -std=c++11 -fopenmp -lgomp
*/

#define SIZE_GB 2.0

#include <mpi.h>
#include <omp.h>

using namespace std;

template <typename T>
T function(T x){
  return x*x/2.0;
}

template <typename T, int n>
void laplacjan_matrix(T *A, int ip, int np){
  int m = n/np;
  size_t size = sizeof(double)*n *m;;
  //memset(A, 0, size);
  for(int i = ip*m; i < (ip+1)*m; i++){
    for(int j = 0 ; j < n; j++){
      int it = i-ip*m + j*n;
      if(i == j) A[it] = -2;
      else if(abs(i-j) == 1) A[it] = 1;
      else A[it] = 0;
    }
  }
}

template <typename T, int n>
void mv_serial(T *A, T *B, T *C, int ip, int np){
  int m = n/np;
  for(int i = ip*m; i < (ip+1)*m; i++){
    C[i] = 0;
    for(int j = 0 ; j < n; j++){
      int it = i-ip*m + j*n;
      C[i] += A[it]*B[j];
    }
  }
}

template <typename T, int n>
void mv_openmp(T *A, T *B, T *C, int ip, int np){
  int m = n/np;
  for(int i = ip*m; i < (ip+1)*m; i++){
    C[i] = 0;
    #pragma omp parallel for reduction(+:C[i])
    for(int j = 0 ; j < n; j++){
      int it = i-ip*m + j*n;
      C[i] += A[it]*B[j];
    }
  }
}

#define N 128

int main(int argc, char *argv[]){
  double t1, t2, t3;
  int ip, np, m;
  MPI_Init( &argc , &argv );
  MPI_Comm_size( MPI_COMM_WORLD , &np );
  MPI_Comm_rank( MPI_COMM_WORLD , &ip );

  m = N/np;
  size_t size = sizeof(double)*N *m;;
  double *A, *B, *X;
  A = (double *) malloc(size);
  B = (double *) malloc(sizeof(double)*N);
  X = (double *) malloc(sizeof(double)*N);
  for(int i = 0; i < N; i++){
    B[i] = function<double>((double)(i-N/2));
  }
  laplacjan_matrix<double,N>(A, ip, np);
  MPI_Barrier(MPI_COMM_WORLD);
  t1 = MPI_Wtime();
  //mv_serial<double, N>(A, B, X, ip, np);
  mv_openmp<double, N>(A, B, X, ip, np);
  t2 = MPI_Wtime() - t1;
  MPI_Gather(X+ip*m, m, MPI_DOUBLE, X, m, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  t3 = MPI_Wtime() -t2 - t1;
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Offset offset; MPI_File fh ; MPI_Status status ;
  offset = N *m;
  //MPI_File_open (MPI_COMM_WORLD, "mpi.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  MPI_File_open (MPI_COMM_WORLD, "openMP.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  MPI_Offset displace = offset * ip *sizeof(double);
  MPI_File_set_view (fh , displace , MPI_DOUBLE, MPI_DOUBLE, "native" ,MPI_INFO_NULL);

  MPI_File_write(fh, A, offset, MPI_DOUBLE, &status);

  MPI_File_close(&fh);

  if (ip == 0){
    cout<<"Czas II: "<<t2<<" , czas III: "<<t3<<endl;
    //FILE *fp = fopen("result_MPI.dat", "w");
    FILE *fp = fopen("result_openMP.dat", "w");
    fwrite(X, sizeof(double), N, fp);
    fclose(fp);
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  return 0;
}
